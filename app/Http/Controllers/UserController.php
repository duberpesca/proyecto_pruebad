<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Session;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Session::exists("user_login")){
            return view('users.index');
        }else{
            return view("users.login");    
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Session::exists("user_login")){
            return view('users.create');
        }else{
            return view('users.registration');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->input('_vr1')=="log_0001"){
            $is_loged = User::login($request->input('email'),$request->input('password'),$request->input('is_admin'));
            if($is_loged){
                return view('users.index');
            }else{
                
                return redirect('/');//falta mostrar mensaje ver como al final y validar 
            }
            
          
        }else {
            $user = new User();
           if($request->hasFile("avatar")){
                //este codigo es para guardar una imagen en la carpeta public/images
                $file = $request->file("avatar");
                $name= time().$file->getClientOriginalName();
                $file->move(public_path()."/images/",$name);
                $user->avatar =$name;
            }else{
                $user->avatar ="default-user.png";
            }
            $user->name = $request->input('name');
            $user->lastname = $request->input('lastname');
            $user->dni = $request->input('dni');
            $user->email = $request->input('email');
            $user->password = $request->input('password');
            if($request->input('_vr1')=="reg0001"){//esto es poque viene de formulario registro entonces nunca va a ser admin a menos que venga de otro form
                $user->is_admin_user = false;
            }else{
                $user->is_admin_user=(boolean)$request->input("is_admin");
            }
            $user->estado ="Activo";
            $user->remember_token=$request->input("_token");
            $user->save();     
        }   
       
        
       return redirect('/');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
