<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Reserva;
use Illuminate\Support\Facades\Session;
class ReservaController extends Controller
{
    public function new_update_ajax(Request $request, $nombre){
         $arr1=$request->only('id_butaca','fecha_reserva');
            $fech1= explode("/", $arr1['fecha_reserva']);
            $str1=$fech1[2]."/".$fech1[1]."/".$fech1[0];
            $listaUsuarios[]="<option value=''>--Seleccione--</option>";
         $reserva_usuario = Reserva::where("fecha_asistencia",$str1)->where("id_butaca",$arr1['id_butaca'])->get();
            $butacaEstadoFecha="Reservada";

            if(count($reserva_usuario)==0){
                $butacaEstadoFecha="Disponible";
                $usuarios = User::where("estado","Activo")->get();
                
                foreach ($usuarios as $key => $usuario) {
                    $res_us = Reserva::where("fecha_asistencia",$str1)->where("id_usuario_reserva",$usuario->id)->get();
                    if(count($res_us)==0){
                        $listaUsuarios[]="<option value='".$usuario->id."'>".$usuario->name." ".$usuario->lastname."</option>";
                    }
                }

            }
           
            return  json_encode(array('msg' =>$butacaEstadoFecha,"lista_usuarios"=>$listaUsuarios));

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('reservas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $fech1= explode("/", $request->input("fecha_reserva"));
            $str1=$fech1[2]."/".$fech1[1]."/".$fech1[0];
        $reserva = new Reserva();
        $reserva->fecha_asistencia=$str1;
        $usuarioEnSession = Session::get("user_login");
        $reserva->id_usuario_titular=$usuarioEnSession->id;
        $reserva->id_usuario_reserva = $request->input("usuario_reserva");
        $reserva->id_butaca=$request->input('id_butaca');
        $reserva->save();
        return redirect("/reservas");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
