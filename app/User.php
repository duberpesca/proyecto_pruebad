<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Session;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public static function login ($email,$password,$is_admin){
        if(Session::exists("user_login")){
                Session::forget('user_login');
        }
        $users = User::where("email",$email)->where("password",$password)->where("Estado","Activo")->get();
        foreach ($users as $user) {
                
                if((boolean)$is_admin ==true ){
                    if((boolean)$user->is_admin_user ==true){
                         $user->is_admin_user=(boolean)$is_admin;
                    Session::put('user_login', $user);       
                    }else{

                    }
                   
                }else{
                    if((boolean)$user->is_admin_user ==true){
                           $user->is_admin_user=(boolean)$is_admin;
                    Session::put('user_login', $user);
                    }else{
                        $user->is_admin_user=(boolean)$is_admin;
                    Session::put('user_login', $user);
                    }
                }
               
          }  
        
        if(Session::exists("user_login")){
            return true;
        }else{
            return false;
        }
    return $user;
    }
}
