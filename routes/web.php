<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource("/","UserController");
Route::resource("users","UserController");
Route::get("/logout",function (){
		if(Session::exists("user_login")){
                Session::forget('user_login');
        }
	return redirect('/');
});
Route::get("butacas/{nombre}","ButacaController@new_update_ajax");
Route::resource("butacas","ButacaController");
Route::get("reservas/{nombre}","ReservaController@new_update_ajax");//siempre colocar las llamadas get antes de las resoruce
Route::resource("reservas","ReservaController");