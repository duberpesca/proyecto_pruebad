@extends('layouts.app')
@section('title','Crear Usuario')
@section('content')
<?php use Illuminate\Support\Facades\Session;?>
 <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Crear Usuario</h1>
                </div>
                <!-- /.col-lg-12 -->
           
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <h5>Usuarios</h5>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-15">
            
<form class="" role="form" method="POST" action="/users" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="_vr1" value="reg0002">
      
      <table style="margin: 0 auto;" id="tb1">
        <tbody>
          <tr>
            <td class="ancho_td">
                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" class="form-control" name="name" placeholder="Nombres">
                </div> 
            </td>
            <td class="td_espacio"></td>
            <td class="ancho_td">
               <div class="form-group">
                  <label>Apellidos</label>
                  <input type="text" class="form-control" name="lastname" placeholder="Apellidos">
               </div>  
            </td>
          </tr>
          <tr>
            <td class="ancho_td">
               <div class="form-group">
                    <label>DNI</label>
                    <input type="text" class="form-control" name="dni" placeholder="DNI">
                </div>        
            </td>
           <td class="td_espacio"></td>
            <td class="ancho_td">
              <div class="form-group">
                 <label>Email</label>
                   <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email" required>
               </div>   
            </td>
          </tr>
          <tr>
            <td class="ancho_td">
              <div class="form-group">
          <label>Password</label>
          <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
        </div>
            </td>
            <td class="td_espacio"></td>
            <td class="ancho_td">
              <div class="form-group">
          <label>Avatar</label>
          <input type="file" name="avatar" id="avatar" class="form-control" accept="image/*">
        </div>
            </td>
          </tr>
          <tr>
             <td class="ancho_td">
               <div class="form-group">
          <img id="img_icon" class="card-img-top rounded-circle mx-auto d-block" src="/images/default-user.png" alt="Icon" style="height: 100px;width: 108px;background-color: #EFEFEF;margin: 20px;"><br>
<div class="checkbox mb-3">
        <label>
          <p><?php Session::get('user_login')->is_admin_user?></p>
          <input type="checkbox" name="is_admin"  value="false"> Es Administrador
        </label>
      </div>
        </div>
             </td>
             <td class="td_espacio"></td>
             <td class="ancho_td" style="text-align: center;">
                <button class="btn btn-primary"  type="submit">Guardar</button>               
             </td>
          </tr>
          
        </tbody>
      </table>
      
      
      

        
      
      
    </form>
  
                                </div>
                                
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
                               
	   </div>
    <style type="text/css">

     
      
      .ancho_td{
        width: 50%;
      }
      .td_espacio{
        width:1%;
      }
    </style>
    <script type="text/javascript">
  $("#avatar").change(function(evt){


      readURL(this);
  });
  function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#img_icon').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}
</script>
@endsection