<!DOCTYPE html>
<?php use Illuminate\Support\Facades\Session; ?>
<html lang="{{ app()->getLocale() }}">
    <head>
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">        
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../../../favicon.ico">

        <title>Teatro Web - @yield('title')</title>

        <!-- Bootstrap core CSS -->

        
        <!-- Custom styles for this template -->
        <link href="/css/signin.css" rel="stylesheet">
        <!--

        <script src="/js/jquery-3.3.1.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        con estas lineas adelante cargo css y js
        -->

<script src="/boostrapfiles/vendor/jquery/jquery.min.js"></script>        
 <link href="/boostrapfiles/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="/boostrapfiles/vendor/bootstrap/js/bootstrap.min.js"></script>        
        
        

    </head>
    <body background="/images/teatro.png" style="font-size: 150%;">    

        <div class="container text-center">
            @yield('content')
        </div>

    </body>
    
</html>
