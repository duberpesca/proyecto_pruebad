@extends('layouts.app')
@section('title','Reservar')
@section('content')
<?php use App\Butaca;
        use App\User;
    $butacas = Butaca::orderBy('fila')->orderBy('columna')->get();
    $usuarios = User::where("estado","Activo")->get();

    $var_selects = "";
    foreach ($usuarios as $key => $usuario) {
         $var_selects=$var_selects."<option value='".$usuario->id."'>".$usuario->name." ".$usuario->lastname."</option>";
     } 

 ?>
     <!-- este div con class container me sirvio para centrar todo wee-->

            
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Reservas</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <form method="POST" action="/butacas">  
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" <?= (count($butacas)!=0)? "disabled":"" ?> class="btn btn-success">Cargar Butacas<img src="/images/add-icon.png" height="10px;" width="10px"></button>
                            </form>

                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-15">
                                    <table>
                                        <?php $x1=0 ?> 
                                        @foreach($butacas as $butaca)
                                        <?php $x1=$x1+1; ?>
                                        <?php if($x1==1){?>
                                        <tr>
                                        <?php }?>                                            
                                        <td>
                                            <div class='col-sm-13'>
                                            <div class="form-group" >
                                                    <img src="/images/img-butaca2.jpg"  width="50px" height="50px"><span>{{$butaca->fila}}.{{$butaca->columna}}</span><br>
                     <form method="POST" action="/reservas">                               
                                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                 <input type="hidden" name="id_butaca" value="{{$butaca->id}}">                
                <div class='input-group date datetimepicker_convert'>
                    <input type='text' class="form-control fecha_reservacl" id="fch{{$butaca->id}}" name="fecha_reserva" data-idbutaca="{{$butaca->id}}" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
         <select class="form-control" id="sl{{$butaca->id}}" name="usuario_reserva" required>
             <option value="">--Seleccione--</option>
         </select>
<button  class="btn btn-primary" type="submit">Reservar</button>
            </form>
                                            </div>
                                        </div>
                                        </td>
                                        <?php if($x1==7){ ?>
                                        </tr>
                                        <?php $x1=0;} ?>
                                                
                                        @endforeach

                                        
                                       
                                    </table>

                                </div>
                                
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

        
        <script type="text/javascript">

             $(function () {


                $('.datetimepicker_convert').datetimepicker({
                    format: 'DD/MM/YYYY',
                    locale:'es'

                }).on('dp.change',function (ev){
                    var idButaca=$(this).find("input").data("idbutaca");
                    var fechaReserva=$(this).find("input").val();
                    validar_reserva_fecha(idButaca,fechaReserva);
                   
                });

                
            });
            

           function validar_reserva_fecha(idButaca,fechaReserva){
        var clientesopts='<option value="">--Seleccione--</option>';

                    $.get('/reservas/new_update_ajax',{id_butaca:idButaca,fecha_reserva:fechaReserva},function(msg){
                       
                         if(msg.msg=="Reservada"){
                            alert("Esta butaca esta reservada para el "+fechaReserva);
                        }else{
                            clientesopts= msg.lista_usuarios;
                        }

                        

                        $("#sl"+idButaca) .find('option')
                                            .remove()
                                            .end()
                                            .append(clientesopts);
                        

                        
                    },'json');     
           }
           function reservar(evt){
                
           }   
                 
        </script>
<style type="text/css">
    td{
        width: 200px;
        height: 100px;
    }
</style>
@endsection