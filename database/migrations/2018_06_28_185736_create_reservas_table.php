<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_asistencia');
            $table->integer('id_usuario_titular')->unsigned();  //creo campo      
            $table->foreign('id_usuario_titular')->references('id')->on('users'); //asigno referencia
            $table->integer('id_usuario_reserva')->unsigned();  //creo campo      
            $table->foreign('id_usuario_reserva')->references('id')->on('users'); //asigno referencia
            $table->integer('id_butaca')->unsigned();
            $table->foreign('id_butaca')->references('id')->on('butacas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas');
    }
}
